void main() {
  var FNB = new winner();
  FNB.name = "FNB".toUpperCase();
  FNB.year = 2012;
  FNB.developer = "Farren Roper";
  FNB.category = "Best Android App";

  var snapScan = new winner();
  snapScan.name = "SnapScan".toUpperCase();
  snapScan.year = 2013;
  snapScan.developer = "Kobus Ehlers";
  snapScan.category = "Best HTML 5 App";

  var liveInspector = new winner();
  liveInspector.name = "live Inspector".toUpperCase();
  liveInspector.year = 2014;
  liveInspector.developer = "Jasper Van Heesch";
  liveInspector.category = "Best Enterprise solution";

  var WumDrop = new winner();
  WumDrop.name = "Wum Drop".toUpperCase();
  WumDrop.year = 2015;
  WumDrop.developer = "Benjamin Claassen";
  WumDrop.category = "Best Enterprise Solution";

  var Domestly = new winner();
  Domestly.name = "Domestly".toUpperCase();
  Domestly.year = 2016;
  Domestly.developer = "Thato Marumo";
  Domestly.category = "Best Consumer App";

  var Shyft = new winner();
  Shyft.name = "Shyft".toUpperCase();
  Shyft.year = 2017;
  Shyft.developer = "Fikile Kgobe";
  Shyft.category = "Best Financial Solution";

  var Khula = new winner();
  Khula.name = "Khula".toUpperCase();
  Khula.year = 2018;
  Khula.developer = "Ayanda Mbonani-Vana";
  Khula.category = "Best Agricultural solution";

  FNB.printOverallWinner();
  snapScan.printOverallWinner();
  liveInspector.printOverallWinner();
  WumDrop.printOverallWinner();
  Domestly.printOverallWinner();
  Shyft.printOverallWinner();
  Khula.printOverallWinner();
}

class winner {
  String? name;
  int? year;
  String? developer;
  String? category;
  void printOverallWinner() {
    print(name);
    print(year);
    print(developer);
    print(category);
  }
}
